<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language; ?>" xml:lang="<?php print $language; ?>">
<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?><?php print $styles; ?><?php print $scripts; ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body class="<?php print $layout; ?>">
<div id="wrapper">
  <?php if ($site_slogan): ?>
  <!-- <?php if($is_front) { //  if we are on the homepage, print the slogan ?>
  <span class="header_title"><?php print $site_slogan; ?></span>
  <?php } /* else { // otherwise print the page title ?>
  <span class="header_title"><?php print drupal_get_title(); ?></span>
  <?php } */ endif; ?> -->
  <div id="header">
    <?php if ($logo): ?>
      <a href="<?php print base_path(); ?>" title="<?php print $site_name; ?>"><img src="<?php print $logo; ?>" alt="<?php if ($site_name): print $site_name;  endif; ?>" class="logo" /></a>
    <?php endif; ?>
    <?php if ($site_name): ?>
      <span class="site_name"> <a href="<?php print base_path(); ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a> </span>
    <?php endif; ?>
    <?php if(variable_get('site_slogan', '')): ?>
      <span class="slogan"><?php print variable_get('site_slogan', ''); ?></span>
    <?php endif; ?>
    <div class="navigation">
      <?php print theme('links', $primary_links); ?>
    </div>
    <?php if ($secondary_links): ?>
      <div class="secondary_links"> <?php print theme('links', $secondary_links); ?></div>
    <?php endif; ?>
  </div>
  <div class="container">
    <div id="page">
      <div id="content">
        <?php if ($breadcrumb): ?>
        <div class="breadcrumb"><?php print $breadcrumb; ?></div>
        <?php endif; ?>
        <?php if ($messages): ?>
        <?php print $messages; ?>
        <?php endif; ?>
        <?php if ($is_front && $mission): ?>
        <div class="mission"><?php print $mission; ?></div>
        <?php endif; ?>
        <?php if ($contenttop): ?>
        <div id="content_top"><?php print $contenttop; ?></div>
        <?php endif; ?>
        <?php if ($title): ?>
        <h1 class="title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php if ($help): ?>
        <div class="help"><?php print $help; ?></div>
        <?php endif; ?>
        <?php if ($tabs): ?>
        <?php print $tabs; ?>
        <?php endif; ?>
        <?php print $content; ?>
        <?php if ($contentbottom): ?>
        <div id="content_bottom"><?php print $contentbottom; ?></div>
        <?php endif; ?>
      </div>
    </div>
    <div id="sidebar">
    <?php if ($sidebar_left): ?>
    <div class="left"><?php print $sidebar_left; ?></div>
    <?php endif; ?>
		<?php if ($sidebar_right): ?>
    <div class="right"><?php print $sidebar_right; ?></div>
    <?php endif; ?>
    </div>
  </div>
  <div class="push">&nbsp;</div>
</div>
<div id="footer-wrapper">
<div id="footer"><?php print $contentfooter; ?>
  <?php if ($footer_message): ?>
  <?php print $footer_message; ?>
  <?php endif; ?>
</div>
</div>
<?php print $closure; ?>
</body>
</html>
