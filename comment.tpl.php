<div id="comment-<?php print $comment->cid; ?>" class="comment<?php print ($comment->new)?" comment-new":""; ?>">
  <?php if ($title): ?>
  <span class="title"> <?php print $title; ?>
  <?php if ($comment->new): ?>
  <span class="new"><?php print $new; ?></span>
  <?php endif; ?>
  </span> 
  <div class="content"><?php print $content; ?></div>
  <?php endif; ?>
  <?php if ($submitted): ?>
  <span class="info"><?php print $picture ?> <?php print 'Posted by ' .  $author; ?> on <?php print $date; ?> </span> 
	<?php endif; ?>
  <?php print $links; ?> </div>