<?php
function skymod_regions() {
  return array(
       'header' => t('header'),
       'left' => t('sidebar left'),
       'right' => t('sidebar right'),
       'contenttop' => t('content top'),
       'contentbottom' => t('content bottom'),
       'contentfooter' => t('content footer'),
       'footer' => t('footer')
  );
}

function phptemplate_links($links, $attributes = array('class' => 'links')) {
	 	  $output = '';

	 	  if (count($links) > 0) {
	 	    $output = '<ul'. drupal_attributes($attributes) .'>';

	 	    $num_links = count($links);
	 	    $i = 1;

	 	    foreach ($links as $key => $link) {
	 	      $class = '';

	 	      // Automatically add a class to each link and also to each LI
	 	      if (isset($link['attributes']) && isset($link['attributes']['class'])) {
	 	        $link['attributes']['class'] .= ' ' . $key;
	 	        $class = $key;
	 	      }
	 	      else {
	 	        $link['attributes']['class'] = $key;
	 	        $class = $key;
	 	      }

	 	      // Add first and last classes to the list of links to help out themers.
	 	      $extra_class = '';
	 	      if ($i == 1) {
	 	        $extra_class .= 'first ';
	 	      }
	 	      if ($i == $num_links) {
	 	        $extra_class .= 'last ';
	 	      }
	 	      $output .= '<li class="'. $extra_class . $class .'">';

	 	      // Is the title HTML?
	 	      $html = isset($link['html']) && $link['html'];

	 	      // Initialize fragment and query variables.
	 	      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
	 	      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

	 	      if (isset($link['href'])) {
	 	        $output .= l('<span>' . $link['title'] . '</span>', $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, TRUE);
	 	      }
	 	      else if ($link['title']) {
	 	        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
	 	        if (!$html) {
	 	          $link['title'] = check_plain($link['title']);
	 	        }
	 	        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
	 	      }

	 	      $i++;
	 	      $output .= "</li>\n";
	 	    }

	 	    $output .= '</ul>';
	 	  }

	 	  return $output;
	 	}

function _phptemplate_variables($hook, $vars = array()) {
  switch ($hook) {
    case 'page':
      $vars['body_class'] = '';

      $layout = '';
      //Is there a left sidebar?
      if ($vars['sidebar_left'] != '') {
          $layout = 'sidebar-left';
      }
      //Is there a right sidebar?
      if ($vars['sidebar_right'] != '') {
           $layout = ($layout == 'sidebar-left') ? 'sidebars' : 'sidebar-right';
      }
      //Put layout into body_class
      if ($layout != ''){
         $vars['body_class'] .= ' ' . $layout;
      }
      break;
  }
  return $vars;
}

?>